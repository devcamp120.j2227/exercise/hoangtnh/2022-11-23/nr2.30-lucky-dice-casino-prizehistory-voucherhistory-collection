//import thư viện express Js
const express = require("express");

const router = express.Router();

const prizeController = require("../controllers/prizeController");

router.post("/prizes", prizeController.createPrize);
router.get("/prizes", prizeController.getAllPrizes);
router.get("/prizes/:prizeId", prizeController.getPrizeById);
router.put("/prizes/:prizeId", prizeController.updatePrizeById);
router.delete("/prizes/:prizeId", prizeController.deletePrizeById);

module.exports = router;